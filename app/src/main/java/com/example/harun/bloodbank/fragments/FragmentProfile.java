package com.example.harun.bloodbank.fragments;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.harun.bloodbank.MainPage;
import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.database.Database;
import com.example.harun.bloodbank.interfaces.ProfileInterface;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentProfile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProfile extends Fragment {
    View v;
    Dialog myDialog;
    public FragmentProfile() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_fragment_profile, container, false);
        ((Button)v.findViewById(R.id.findBlood)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog = new Dialog(getActivity());
                ShowPopup();
                return;
            }
        });

        ((Button)v.findViewById(R.id.editProfile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileInterface pi = (ProfileInterface) getActivity();
                pi.replaceFragment("editProfile");
            }
        });
        ((Button)v.findViewById(R.id.findingBlood)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileInterface pi = (ProfileInterface) getActivity();
                pi.replaceFragment("findingBlood");
            }
        });
        return v;
    }

    public void ShowPopup(){

        myDialog.setContentView(R.layout.custompopup);
        TextView tv = (TextView)myDialog.findViewById(R.id.close);
        final TextView fullName = (TextView)myDialog.findViewById(R.id.fullName);
        final TextView phoneNumber = (TextView)myDialog.findViewById(R.id.phoneNumber);
        final Spinner city = (Spinner)myDialog.findViewById(R.id.city);
        final Spinner town = (Spinner)myDialog.findViewById(R.id.town);
        final Spinner blood = (Spinner)myDialog.findViewById(R.id.bloodType);
        Button btn = (Button)myDialog.findViewById(R.id.startFinding);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database db = new Database(getActivity());
                db.insertData(new WantedBlood(fullName.getText().toString(),
                        phoneNumber.getText().toString(),
                        city.getSelectedItem().toString(),
                        town.getSelectedItem().toString(),
                        blood.getSelectedItem().toString()
                        ));
                ProfileInterface pi = (ProfileInterface) getActivity();
                pi.replaceFragment("findingBlood");
                myDialog.dismiss();
            }
        });

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

}
