package com.example.harun.bloodbank.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.interfaces.LoginInterface;


public class FragmentLogin extends Fragment {

    public FragmentLogin() {
        // Required empty public constructor
    }
    Button btnGiris;
    EditText username,pw;
    TextView tvRegister,tvRememberPassword;
    TextInputLayout inputLayoutUsername,inputLayoutPassword;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_login2, container, false);

        /**Btn & EditText*/
        btnGiris = (Button)v.findViewById(R.id.Giris);
        username = (EditText)v.findViewById(R.id.username);
        pw = (EditText)v.findViewById(R.id.pw);

        inputLayoutUsername = (TextInputLayout)v.findViewById(R.id.inputLayoutUserName);
        inputLayoutPassword = (TextInputLayout)v.findViewById(R.id.inputLayoutPassword);
        /*IntpuLayoutNames*/



        btnGiris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userNameControl() && passwordControl()) {
                    if(username.getText().toString().equals("hcakir")  && pw.getText().toString().trim().equals("harun123")) {
                        Toast.makeText(getActivity(),"Giriş Başarılı",Toast.LENGTH_SHORT).show();
                        LoginInterface li = (LoginInterface) getActivity();
                        li.goMainPage();
                    }

                    else if(username.getText().toString().equals("berkay5011")  && pw.getText().toString().trim().equals("berkay5011")) {
                        Toast.makeText(getActivity(), "Giriş Başarılı", Toast.LENGTH_SHORT).show();
                        LoginInterface li = (LoginInterface) getActivity();
                        li.goMainPage();
                    }
                    else {
                        Toast.makeText(getActivity(),"Giriş Başarısız",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ((TextView)v.findViewById(R.id.registerTV)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginInterface li = (LoginInterface) getActivity();
                li.goRegister();
            }
        });


        ((TextView)v.findViewById(R.id.remmeberPW)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginInterface li = (LoginInterface) getActivity();
                li.goRememberPassword();
            }
        });

        return v;
    }

    private boolean userNameControl(){
        if(username.getText().toString().isEmpty()) {
            inputLayoutUsername.setError("Kullanıcı adı alanı boş olamaz");
            return false;
        } else {
            inputLayoutUsername.setErrorEnabled(false);
            return true;
        }
    }

    private boolean passwordControl(){
        if(pw.getText().toString().isEmpty()) {
            inputLayoutPassword.setError("Şifre alanı boş olamaz");
            return false;
        } else if(pw.getText().toString().trim().length() < 8) {
            inputLayoutPassword.setError("Şifre alanı minimum 8 karakter olmalıdır");
            return true;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
            return true;
        }
    }








}
