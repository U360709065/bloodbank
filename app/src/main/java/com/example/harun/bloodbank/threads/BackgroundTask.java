package com.example.harun.bloodbank.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.adapters.MainAdapter;

import java.util.ArrayList;
import java.util.List;

public class BackgroundTask extends AsyncTask<Void,WantedBlood,Void> {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    Context context;
    private MainAdapter adapter;
    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();
    List<WantedBlood> geciciListe = new ArrayList<WantedBlood>();

    public BackgroundTask(RecyclerView recyclerView,ProgressBar progressBar,Context context){
        geciciListe.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        geciciListe.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        geciciListe.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        geciciListe.add(new WantedBlood("Kaan Burkay","0535 021 40 33","Bursa","İnegöl","AB RH+"));
        geciciListe.add(new WantedBlood("Muammer Akal","0531 432 26 19","Malatya","Kayısısı","0 RH+"));
        geciciListe.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        geciciListe.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        geciciListe.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        geciciListe.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        geciciListe.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        geciciListe.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        geciciListe.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        geciciListe.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        geciciListe.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        geciciListe.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));

        this.recyclerView = recyclerView;
        this.progressBar = progressBar;
        this.context = context;
    }


    @Override
    protected void onPreExecute() {
        adapter = new MainAdapter(context,wantedBloodList);
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (int i =0; i<geciciListe.size()-1;i++){
            publishProgress(((WantedBlood)geciciListe.get(i)));
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    protected void onProgressUpdate(WantedBlood... values) {
            wantedBloodList.add(values[0]);
            adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progressBar.setVisibility(View.GONE);
    }
}
