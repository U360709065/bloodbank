package com.example.harun.bloodbank.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.harun.bloodbank.CustomAdapter;
import com.example.harun.bloodbank.MainPage;
import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.adapters.MainAdapter;
import com.example.harun.bloodbank.threads.BackgroundTask;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMainPage extends Fragment {
    View v;
    ListView liste;
    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();
    MainAdapter adapter;

    RecyclerView recyclerView;

    ProgressBar progressBar;

    public FragmentMainPage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_fragment_main_page, container, false);
        //liste = (ListView)v.findViewById(R.id.lvBloods);
        //CustomAdapter adaptor = new CustomAdapter(getActivity(),wantedBloodList);
        //liste.setAdapter(adaptor);
        recyclerView = v.findViewById(R.id.recyclerView);
        adapter = new MainAdapter(getActivity(),wantedBloodList);
        //recyclerView.setAdapter(adapter);
        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return v;
    }

    public void sendData(List<WantedBlood> wantedBloodList) {
        this.wantedBloodList = wantedBloodList;

    }

    @Override
    public void onResume() {
        super.onResume();
        new BackgroundTask(recyclerView,progressBar,getActivity()).execute();
    }
}
