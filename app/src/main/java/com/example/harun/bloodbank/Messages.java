package com.example.harun.bloodbank;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Messages extends AppCompatActivity {
    Dialog myDialog;

    ListView liste;
    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                Intent url;
                if(menuItem.getItemId() == R.id.profile) {
                    url = new Intent(Messages.this,Profile.class);
                    startActivity(url);
                    return;
                } else if(menuItem.getItemId() == R.id.messages){
                    url = new Intent(Messages.this,Messages.class);
                    startActivity(url);
                    return;
                }else {
                    myDialog = new Dialog(Messages.this);
                    ShowPopup();
                    return;
                }

            }
        });
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Kaan Burkay","0535 021 40 33","Bursa","İnegöl","AB RH+"));
        wantedBloodList.add(new WantedBlood("Muammer Akal","0531 432 26 19","Malatya","Kayısısı","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));
        wantedBloodList.add(new WantedBlood("Harun Çakır","0535 061 80 37","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Berkay Özkan","0543 241 13 93","Antalya","Kepez","0 RH+"));
        wantedBloodList.add(new WantedBlood("Mehmet Ali Bayır","0524 465 23 15","Hatay","İskenderun","A RH-"));


        liste = (ListView)findViewById(R.id.lvBloods);

        CustomAdapter adapter = new CustomAdapter(Messages.this,wantedBloodList);
        liste.setAdapter(adapter);
    }

    public void ShowPopup(){

        myDialog.setContentView(R.layout.custompopup);
        TextView tv = (TextView)myDialog.findViewById(R.id.close);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }
}
