package com.example.harun.bloodbank.fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.Profile;
import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.interfaces.LoginInterface;
import com.example.harun.bloodbank.interfaces.ProfileInterface;

import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEditProfile extends Fragment implements View.OnClickListener {
    TextView tvNameSurname,tvEmail,tvPassword,tvPhoneNumber;
    TextInputLayout tiNameSurname,tiEmail,tiPassword,tiPhonenNumber;
    View v;

    public FragmentEditProfile() {
        // Required empty public constructor







    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_fragment_edit_profile, container, false);
        loadData();
        ((Button)v.findViewById(R.id.formControl)).setOnClickListener(this);

        return v;
    }
    private void loadData() {
        tvNameSurname = (TextView) v.findViewById(R.id.tvNameSurName);
        tvEmail = (TextView)v.findViewById(R.id.tvEmail);
        tvPassword = (TextView)v.findViewById(R.id.tvPassword);
        tvPhoneNumber = (TextView)v.findViewById(R.id.tvPhone);

        tiNameSurname = (TextInputLayout)v.findViewById(R.id.inputlayoutNameSurName);
        tiEmail = (TextInputLayout)v.findViewById(R.id.inputlayoutEmail);
        tiPassword = (TextInputLayout)v.findViewById(R.id.inputlayoutPassword);
        tiPhonenNumber = (TextInputLayout)v.findViewById(R.id.inputlayoutPhone);
    }
    private boolean NameSurnameControl() {
        if(tvNameSurname.getText().toString().isEmpty()){
            tiNameSurname.setError("Ad soyad alanı boş bırakılamaz");
            return false;
        }else {
            tiNameSurname.setErrorEnabled(false);
            return true;
        }
    }

    private boolean EmailControl() {
        if(tvEmail.getText().toString().isEmpty()){

            tiEmail.setError("E mail alanı boş bırakılamaz");
            return false;
        }else if(!isValidEmailId(tvEmail.getText().toString().trim())){
            tiEmail.setError("Geçerli bir email adresi giriniz");
            return false;
        }else {
            tiEmail.setErrorEnabled(false);
            return true;
        }
    }

    private boolean PasswordControl() {
        if(tvPassword.getText().toString().isEmpty()){

            tiPassword.setError("Password alanı boş bırakılamaz");
            return false;
        }else if(tvPassword.getText().toString().trim().length() < 8){
            tiPassword.setError("Şifre alanı min 8 karakter olmalıdır");
            return false;
        }else {
            tiPassword.setErrorEnabled(false);
            return true;
        }
    }

    private boolean PhoneControl() {
        if(tvPhoneNumber.getText().toString().isEmpty()){
            tiPhonenNumber.setError("Telefon alanı boş bırakılamaz");
            return false;
        }else {
            tiPhonenNumber.setErrorEnabled(false);
            return true;
        }
    }



    @Override
    public void onClick(View v) {
        if(NameSurnameControl() && EmailControl() && PasswordControl() && PhoneControl()) {
            Toast.makeText(getActivity(),"Profil düzenlendi",Toast.LENGTH_LONG).show();

        }
    }


    private boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }


}
