package com.example.harun.bloodbank.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.harun.bloodbank.CustomAdapter;
import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.adapters.MainAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMessages extends Fragment {
    View v;
    ListView liste;
    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();

    public FragmentMessages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_fragment_messages, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.recyclerView);
        MainAdapter adapter = new MainAdapter(getActivity(),wantedBloodList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return v;
    }

    public void sendData(List<WantedBlood> wantedBloodList) {
        //this.wantedBloodList = wantedBloodList;
        loadData();
    }
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void loadData() {
        wantedBloodList.add(new WantedBlood("Yusuf İlker Oğuz","0525 323 42 56","İstanbul","Kadıköy","AB RH+"));
        wantedBloodList.add(new WantedBlood("Kaan Burkay Kabakçı","0543 241 13 93","İstanbul","Kadıköy","0 RH+"));
        wantedBloodList.add(new WantedBlood("Burhan Çakar","0564 565 23 35","İstanbul","Kadıköy","0 RH+"));


    }
}
